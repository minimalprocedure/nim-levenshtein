#!/bin/bash

export PATH=$HOME/lib/musl/bin:$PATH

export CC=/usr/bin/gcc-9
#export CFLAGS='-O3 -mtune=native -march=native -flto'

rm .cache -Rv

nimble build -d:release --gcc.exe:musl-gcc --gcc.linkerexe:musl-gcc

mv levenshtein levenshtein-musl

rm .cache -Rv

nimble build -d:release --gcc.exe:/usr/bin/gcc-9 --gcc.linkerexe:gcc-9

mv levenshtein levenshtein-libc

strip levenshtein-musl
strip levenshtein-libc
