import os
import tables

proc min3(a, b, c: int) : int = min(a, min(b, c))

proc lev(s, t: string): int =
  proc distance(i, j: int): int =
    if j == 0: i
    elif i == 0: j
    elif s[i - 1] == t[j - 1]:
      distance(i - 1, j - 1)
    else:
      let del = distance(i - 1, j)
      let ins = distance(i, j - 1)
      let sub = distance(i - 1, j - 1)
      min3(del, ins, sub) + 1
  distance(s.len, t.len)

proc memoizeProc2[A, B, C](f: proc(i: A, j: B): C): proc(i: A, j: B): C =
  var table = initTable[(A, B), C]()
  result =
    proc (i: A, j: B): int =
      if table.hasKey((i, j)): table[(i, j)]
      else:
        let r = f(i, j)
        table[(i, j)] = r
        r

proc levm(s, t: string): int =
  var distance: proc (i, j : int): int
  distance = proc(i, j : int): int =
    if j == 0: i
    elif i == 0: j
    elif s[i - 1] == t[j - 1]:
      distance(i - 1, j - 1)
    else:
      let del = distance(i - 1, j)
      let ins = distance(i, j - 1)
      let sub = distance(i - 1, j - 1)
      min3(del, ins, sub) + 1
  distance = memoizeProc2(distance)
  distance(s.len, t.len)

when isMainModule:
  let args = commandLineParams()
  echo levm(args[0], args[1])
