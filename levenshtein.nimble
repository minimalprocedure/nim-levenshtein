# Package

version       = "0.1.0"
author        = "Massimo Maria Ghisalberti"
description   = "Levenshtein distance"
license       = "MIT"
srcDir        = "src"
bin           = @["levenshtein"]

# Dependencies

requires "nim >= 1.0.0"
